import { requestTimeoutConfig } from './config';

describe('requestTimeoutConfig', () => {
  it('defaults requestTimeout.timeoutHttpStatus to 504', () => {
    const config = requestTimeoutConfig.build({ overrides: {} });
    expect(config.requestTimeout.timeoutHttpStatus).toBe(504);
  });

  it('defaults requestTimeout.timeoutInMs to 2500', () => {
    const config = requestTimeoutConfig.build({ overrides: {} });
    expect(config.requestTimeout.timeoutInMs).toBe(2500);
  });

  it('accepts requestTimeout.timeoutHttpStatus and .timeoutInMs', () => {
    const config = requestTimeoutConfig.build({
      overrides: {
        requestTimeout: {
          timeoutHttpStatus: 500,
          timeoutInMs: 10000
        }
      }
    });

    expect(config.requestTimeout.timeoutHttpStatus).toBe(500);
    expect(config.requestTimeout.timeoutInMs).toBe(10000);
  });

  it('fails for invalid values', () => {
    expect(() =>
      requestTimeoutConfig.build({
        overrides: {
          requestTimeout: {
            timeoutHttpStatus: 'talkingtome'
          }
        }
      })
    ).toThrow();

    expect(() =>
      requestTimeoutConfig.build({
        overrides: {
          requestTimeout: {
            timeoutInMs: 'talkingtome2' // too shy to write the next gimmick!
          }
        }
      })
    ).toThrow();
  });
});
