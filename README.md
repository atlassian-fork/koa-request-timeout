# Koa Request Timeout

Request timeout middleware for koa.

Throws RequestTimeoutError providing a given HTTP status when request last longer than specified timing.

## Setup

```typescript
import * as Koa from "koa";

import {
  createRequestTimeoutMiddleware,
  RequestTimeoutError,
  requestTimeoutConfig
} from "@atlassian/koa-request-timeout";

import { baseConfig } from "@atlassian/node-config";

// manage middleware configuration
const configBuilder = baseConfig.concat(requestTimeoutConfig);
const config = configBuilder.build();

const {
  requestTimeout: { timeoutHttpStatus, timeoutInMs }
} = config;

// use middleware
const app = new Koa();
app.use(createRequestTimeoutMiddleware(timeoutInMs, timeoutHttpStatus));
```

## Development

### Build

```bash
    npm test
    npm run build
```

### Publish

```bash
    npm version patch|minor|major
    npm publish
```
